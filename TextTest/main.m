//
//  main.m
//  TextTest
//
//  Created by Brandon Levasseur on 2/5/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
