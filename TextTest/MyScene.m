//
//  MyScene.m
//  TextTest
//
//  Created by Brandon Levasseur on 2/5/14.
//  Copyright (c) 2014 Brandon Levasseur. All rights reserved.
//

#import "MyScene.h"

@implementation MyScene
{
    SKLabelNode *_label;
    CIFilter *_filter;
    
    SKEffectNode *_labelNode;
}

-(id)initWithSize:(CGSize)size {    
    if (self = [super initWithSize:size]) {
        
        self.backgroundColor = [SKColor colorWithRed:0.15 green:0.15 blue:0.3 alpha:1.0];
        
        _labelNode = [SKEffectNode node];
        
        _label = [SKLabelNode labelNodeWithFontNamed:@"Chalkduster"];
        
        _label.text = @"Hello, World!";
        _label.fontSize = 30;
        _label.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
        
        SKLabelNode *newLabel = [SKLabelNode labelNodeWithFontNamed:@"Chalduster"];
        newLabel.text = @"HI";
        newLabel.fontSize = 20;
        newLabel.position = CGPointMake(_label.position.x, _label.position.y - _label.fontSize);;
        
        
        [_labelNode addChild:_label];
        [_labelNode addChild:newLabel];
        
        _filter = [CIFilter filterWithName:@"CICrop"];
        [_filter setValue:[CIVector vectorWithX:0 Y:0 Z:0 W:size.height] forKey:@"inputRectangle"];
        
        _labelNode.filter = _filter;
        _labelNode.shouldEnableEffects = YES;
        
        [self addChild:_labelNode];
        
        [self addChild:[self butterflyEmitterNodeWithSpeed:30 lifetime:CGRectGetHeight(self.frame) scale:0.2 birthRate:1 color:[SKColor blueColor]]];

    }
    return self;
}

-(void)update:(CFTimeInterval)currentTime {
    CIVector *vector = (CIVector *)[_filter valueForKey:@"inputRectangle"];
    CGPoint labelOrigin = [self convertPoint:_label.frame.origin fromNode:_label];
    
    if (vector.Z <= labelOrigin.x - self.frame.origin.x + CGRectGetWidth(_label.frame)) {
        CGFloat delta = currentTime/600 + vector.Z;
        vector = [CIVector vectorWithX:vector.X Y:vector.Y Z:delta W:vector.W];
        [_filter setValue:vector forKey:@"inputRectangle"];
    }

}

- (SKEmitterNode *)butterflyEmitterNodeWithSpeed:(float)speed lifetime:(float)lifetime scale:(float)scale birthRate:(float)birthRate color:(SKColor *)color
{
    
    
    SKTexture *texture = [SKTexture textureWithImageNamed:@"butterfly"];
    
    SKEmitterNode *emitterNode = [SKEmitterNode new];
    emitterNode.particleTexture = texture;
    emitterNode.particleBirthRate = birthRate;
    emitterNode.particleLifetime = lifetime;
    emitterNode.particleSpeed = speed;
    emitterNode.particleScale = scale;
    emitterNode.particleColor = color;
    emitterNode.particleColorBlendFactor = 0.5;
    emitterNode.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    emitterNode.emissionAngleRange = 360;
    emitterNode.particlePositionRange = CGVectorMake(200, 200);
    
    NSMutableArray *textures = [[NSMutableArray alloc] initWithCapacity:4];
    [textures addObject:texture];
    for (int i = 1; i < 3; i++) {
        NSString *textureName = [NSString stringWithFormat:@"butterfly%i", i];
        SKTexture *texture = [SKTexture textureWithImageNamed:textureName];
        [textures addObject:texture];
    }
    
    
    SKAction *textureAnimation = [SKAction animateWithTextures:textures timePerFrame:0.1];
    emitterNode.particleAction = [SKAction repeatActionForever:[SKAction sequence:@[textureAnimation, [textureAnimation reversedAction]]]];
    
    [emitterNode advanceSimulationTime:lifetime];
    
    return emitterNode;
    
}

@end
